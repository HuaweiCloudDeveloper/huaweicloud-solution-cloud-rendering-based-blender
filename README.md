[TOC]

**解决方案介绍**
===============
该方案采用开源的专业级图形图像软件Blender，结合华为云上极致算力，通过一键自动部署模式，为用户提供专业、强大、高效的云端渲染服务。该方案适用于如下场景：
- 效果图制作。效果图行业的客户对出图速度要求高，华为云面向海报设计、装修装饰、工业设计等提供高性价比的渲染专享资源，助力企业降本增效。
- 游戏、AR/VR制作。渲染过程复杂，精细程度要求极高，任务量大，华为云渲染助力企业提升CG制作渲染速度，打造最佳画面，降低VR/AR硬件成本。
- 影视动画制作。影视动画涉及大量特效制作，对GPU资源需求量极高，周期紧，华为云大规模渲染集群满足超大资源需求。

解决方案实践详情页面地址：https://www.huaweicloud.com/solution/implementations/cloud-rendering-based-blender.html

**架构图**
---------------
![方案架构](./document/cloud-rendering-based-blender.png)

**架构描述**
---------------
该解决方案会部署如下资源：
- 创建云渲染服务器，内置开源Blender软件，提供渲染算力。
- 云渲染服务器绑定弹性公网IP，用户可通过该公网IP提交渲染任务。
此外，您可以通过使用云监控服务来监测弹性云服务器的CPU、内存、磁盘IO和网络等指标，当资源利用率超过阈值时触发告警。
**组织结构**
---------------

``` lua
huaweicloud-solution-cloud-rendering-based-blender
├── huaweicloud-solution-cloud-rendering-based-blender.tf.json -- 资源编排模板
├── userdata
    ├── install_blender.sh -- 脚本配置文件
```
**开始使用**
---------------
**安全组规则修改（可选）**

安全组实际是网络流量访问策略，包括网络流量入方向规则和出方向规则，通过这些规则为安全组内具有相同保护需求并且相互信任的云服务器、云容器、云数据库等实例提供安全保护。
如果您的实例关联的安全组策略无法满足使用需求，比如需要添加、修改、删除某个TCP端口，请参考以下内容进行修改。
- 添加安全组规则：根据业务使用需求需要新开放某个TCP端口，请参考[添加安全组规则](https://support.huaweicloud.com/usermanual-vpc/zh-cn_topic_0030969470.html)添加入方向规则，打开指定的TCP端口。
- 修改安全组规则：安全组规则设置不当会造成严重的安全隐患。您可以参考[修改安全组规则](https://support.huaweicloud.com/usermanual-vpc/vpc_SecurityGroup_0005.html)，来修改安全组中不合理的规则，保证云服务器等实例的网络安全。
- 删除安全组规则：当安全组规则入方向、出方向源地址/目的地址有变化时，或者不需要开放某个端口时，您可以参考[删除安全组规则](https://support.huaweicloud.com/usermanual-vpc/vpc_SecurityGroup_0006.html)进行安全组规则删除。

**获取验证demo**

本示例使用Blender 官网提供的demo 文件进行渲染验证，demo文件可从： https://www.blender.org/download/demo-files 网站下载，选择blender 2.79，如图 选择blender版本所示，下载解压得到splash279文件夹，将其上传到渲染节点。

须知：

ECS中安装的Blender版本为2.79，demo版本要和渲染节点中的Blender版本相同，版本不一致可能出现无法渲染。

图1 选择blender版本
![选择blender版本](./document/readme-image-001.png)

**验证步骤**

1.登录华为云控制台，获取公网IP地址，如图2查看公网IP所示。

图2 查看公网IP
![查看公网IP](./document/readme-image-002.png)

2.打开SFTP工具输入获取的公网IP，连接渲染节点，将获取的demo上传至渲染节点并解压缩，解压缩后文件夹名称为splash279。

3.在渲染节点进入splash279目录，执行渲染命令，返回如图 执行渲染结果所示结果为渲染完成。

渲染命令：blender splash279.blend -b -f 1

图3 执行渲染结果

![执行渲染结果](./document/readme-image-003.png)

